'use strict';
let {Before, After} = require('cucumber');



Before({tags: "@notAngular"}, () => {
    return browser.ignoreSynchronization = true;
});

After({tags: "@notAngular"}, () => {
    return browser.ignoreSynchronization = false;
});

// Before(() => {
//   });

After(() => {
    return browser.executeScript('window.sessionStorage.clear();')
        .then(() => {
            return browser.executeScript('window.localStorage.clear();');
        }).then(() => {
            return browser.manage().deleteAllCookies();
        }).then(() => {
            browser.driver.get("about:blank");
        });

});