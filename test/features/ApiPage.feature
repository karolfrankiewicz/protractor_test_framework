Feature: API Page

  Scenario: Check content table visibility toggle
    Given I go to AngularJS main page
    And I open "DEVELOP" dropdown on top menu
    When I go to "API Reference" link from opened dropdown
    And I see contents container with button
    Then I toggle contents table visibility successfully
    And I toggle contents table visibility successfully

  Scenario: Select Angular version from dropdown
    Given I go to AngularJS main page
    And I open "DEVELOP" dropdown on top menu
    And I go to "API Reference" link from opened dropdown
    When I select "v1.6.10" API version from dropdown
    Then I see "v1.6.10" API version selected