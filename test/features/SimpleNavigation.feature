Feature: Navigation feature

  Scenario: Navigate to Protractor API, check few side menu elements and go back to main page
    Given I go to AngularJS main page
    When I open "DEVELOP" dropdown on top menu
    And I go to "API Reference" link from opened dropdown
    Then I am on "AngularJS API Docs" titled page
    And I check if group exists in side menu
      |ng               |
      |auto             |
      |ngAnimate        |
      |ngAria           |
      |ngComponentRouter|
      |ngCookies        |
      |ngMessageFormat  |
      |ngMessages       |
      |ngMock           |
      |ngMockE2E        |
      |ngParseExt       |
      |ngResource       |
      |ngRoute          |
      |ngSanitize       |
      |ngTouch          |
    When I go to Home page through top menu
    Then I am on AngularJS main page