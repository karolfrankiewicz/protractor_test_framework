'use strict';
let Page = require('astrolabe').Page,
    world = require('../config/world');

let page = Page.create({

    pageTitle: {
        get: () => {
            return element(by.id('angularjs-api-docs'));
        }
    },
    sideMenuGroups: {
        get: () => {
            return element.all(by.repeater('navGroup in currentArea.navGroups'));
        }
    },
    contentsContainer: {
        get: () => {
            return element(by.css('toc-container'));
        }
    },
    contentsToggleButton: {
        get: () => {
            return page.contentsContainer.element(by.css('button'));
        }
    },
    contentsTree: {
        get: () => {
            return page.contentsContainer.element(by.css('toc-tree'));
        }
    },
    apiReferenceMenu: {
        get: () => {
            return element(by.id('navbar-sub'));
        }
    },
    apiVersionDropdown: {
        get: () => {
            return page.apiReferenceMenu.element(by.css('select'));
        }
    },
    apiVersionDropdownOptionToSelect: {
        value: (option) => {
            return page.apiVersionDropdown.all(by.cssContainingText('option', option)).first();
            
        }
    },
    apiVersionSelected: {
        get: () => {
            return page.apiVersionDropdown.element(by.css('[selected]'));
        }
    },
    sideMenuGroupMainItems: {
        get: () => {
            return page.sideMenuGroups.all(by.className('nav-index-group-heading'));
        }
    },

    
    getPageTitleText: {
        value: () => {
            return page.pageTitle.getText();
        }
    },
    sideMenuGroupMainItemsText: {
        value: () => {
            return page.sideMenuGroupMainItems.getText();
        }
    },
    toggleContents: {
        value: () => {
            return page.contentsToggleButton.click();
        }
    },
    contentsTreeVisibility: {
        value: () => {
            return page.contentsTree.isDisplayed();
        }
    },
    apiVersionSelect: {
        value: (option) => {
            return page.apiVersionDropdown.click()
                .then(() => {
                    return page.apiVersionDropdownOptionToSelect(option).click();
                });
        }
    },
    getApiVersionSelectedText: {
        value: () => {
            return page.apiVersionSelected.getText();
        }
    },
});
module.exports = page;