'use strict';
let Page = require('astrolabe').Page,
    world = require('../config/world');

let page = Page.create({

    stageContainer: {
        get: () => {
            return element(by.className('stage-container'))
        }
    },
    angularHeader: {
        get: () => {
            return page.stageContainer.element(by.css('h2'));
        }
    },

    angularHeaderVisibility: {
        value: () => {
            return page.angularHeader.isDisplayed();
        }
    }
});
module.exports = page;