'use strict';
let Page = require('astrolabe').Page;

let page = Page.create({

    menuContainer: {
        get: () => {
            return element(by.id('navbar-main'));
        }
    },
    menuLink: {
        value: (text) => {
            return page.menuContainer.element(by.linkText(text));
        }
    },
    activeDropdownMenu: {
        get: () => {
            return page.menuContainer.element(by.css('.dropdown.open .dropdown-menu'));
        }
    },
    activeDropdownLink: {
        value: (text) => {
            return page.activeDropdownMenu.element(by.linkText(text));
        }
    },
    homePageLink: {
        get: () => {
            return page.menuContainer.element(by.css('[alt=AngularJS]'));
        }
    },

    menuLinkClick: {
        value: (text) => {
            return page.menuLink(text).click();
        }
    },
    activeDropdownLinkClick: {
        value: (text) => {
            return page.activeDropdownLink(text).click();
        }
    },
    homePageLinkClick:{
        value: () => {
            return page.homePageLink.click();
        }
    }
    
});
module.exports = page;