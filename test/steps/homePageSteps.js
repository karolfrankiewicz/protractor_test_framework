'use strict';
let world = require('../config/world'),
    pages = require('../config/pages')

let {When} = require('cucumber');


let homePage = pages().homePage;

When(/^I go to AngularJS main page$/, (callback) => {
    browser.get(browser.baseUrl)
        .then(() => {
            world().expect(homePage.angularHeaderVisibility()).to.eventually.equal(true)
                .and.notify(callback);
        })
});

When(/^I am on AngularJS main page$/, (callback) => {
    world().expect(homePage.angularHeaderVisibility()).to.eventually.equal(true)
        .and.notify(callback);
});