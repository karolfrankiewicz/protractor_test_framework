'use strict';
let world = require('../config/world'),
    pages = require('../config/pages')

let {When} = require('cucumber');


let angularApiReference = pages().angularApiReference;

When(/^I am on "(.*)" titled page$/, (title, callback) => {
    angularApiReference.getPageTitleText()
        .then((text) => {
            world().expect(text.startsWith(title)).equal(true);
        })
        .then(callback);
});

When(/^I check if group exists in side menu$/, (data, callback) => {
    let list = data.raw();
    angularApiReference.sideMenuGroupMainItemsText()
        .then(items => {
            for(let i = 0; i< list.length; i++) {
                world().expect(items.indexOf(list[i][0])).to.be.greaterThan(-1);
            }
        })
        .then(callback);
});

When(/^I see contents container with button$/, (callback) => {
    world().expect(angularApiReference.contentsContainer.isDisplayed()).to.eventually.equal(true);
    world().expect(angularApiReference.contentsToggleButton.isDisplayed()).to.eventually.equal(true).and.notify(callback);
});

When(/^I toggle contents table visibility successfully$/, (callback) => {
    let preToggleVisibility,
        postToggleVisibility;

        angularApiReference.contentsTreeVisibility()
        .then(visibility => {
            preToggleVisibility = visibility;
        })
        .then(() => {
            angularApiReference.toggleContents();
        })
        .then(() => {
            return angularApiReference.contentsTreeVisibility()
        })
        .then(visibility => {
            postToggleVisibility = visibility;
        })
        .then(() => {
            world().expect(preToggleVisibility).equal(!postToggleVisibility);            
        })
        .then(callback)
});

When(/^I select "(.*)" API version from dropdown$/, (option, callback) => {
    angularApiReference.apiVersionSelect(option)
    .then(callback);
});


When(/^I see "(.*)" API version selected$/, (option, callback) => {
    world().expect(angularApiReference.getApiVersionSelectedText()).to.eventually.equal(option).and.notify(callback);
});
