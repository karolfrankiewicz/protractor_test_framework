'use strict';
let world = require('../config/world'),
    pages = require('../config/pages')

let {When} = require('cucumber');


let mainMenu = pages().mainMenu;

When(/^I open "(.*)" dropdown on top menu$/, (text, callback) => {
    mainMenu.menuLinkClick(text)
        .then(callback);
});

When(/^I go to Home page through top menu$/, (callback) => {
    mainMenu.homePageLinkClick()
        .then(callback);
});

When(/^I go to "(.*)" link from opened dropdown$/, (text, callback) => {
    mainMenu.activeDropdownLinkClick(text)
        .then(callback);
});