'use strict';
module.exports = (() => {
    let chai = require('chai'),
        chaiAsPromised = require("chai-as-promised");
    
    chai.use(chaiAsPromised);

    const WAIT_DEFAULT = 5000,
        WAIT_MAX = 60000,
        WAIT_MIN = 2000;

    let expect = chai.expect,
        EC = require('protractor').ExpectedConditions;
     

    return {
        expect: expect,
        EC: EC
    };
});
