'use strict';
exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    framework: 'custom',
    frameworkPath: '../../node_modules/protractor-cucumber-framework',
    specs: '../features/*.feature',

    capabilities: {
      browserName: 'chrome'
    },
    cucumberOpts: {
      require: [
        '../steps/*.js',
        '../support/*.js'
    ],
      format: [
        'json:results.json'
      ]
    },
    baseUrl: 'https://angularjs.org/',

    ignoreUncaughtExceptions: true,

    onPrepare: () => {
      browser.manage().window().maximize();
    }
};