'use strict';
module.exports = (() => {

    let homePage = require('../pages/homePage'),
        mainMenu = require('../pages/elements/mainMenu'),
        angularApiReference = require('../pages/angularApiReference');


    return {
        homePage: homePage,
        mainMenu: mainMenu,
        angularApiReference: angularApiReference
    }
});
