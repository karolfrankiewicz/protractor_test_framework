'use strict';

let gulp = require('gulp');
let cucumber = require('gulp-cucumber');
let protractor = require('gulp-protractor').protractor;
let webdriver = require("gulp-webdriver")
let argv = require('yargs').argv;

gulp.task('test:e2e', () => {
    return gulp.src('wdio.conf.js').pipe(webdriver({
        logLevel: 'verbose',
        waitforTimeout: 10000,
        reporter: 'spec'
    }));
});

gulp.src(["test/features/*.feature"])
    .pipe(protractor({
        configFile: 'test/config/protractor.config.js',
        //args: ['--baseUrl', 'https://angularjs.org/']
    }))
    .on('error', (e) => { throw e });
