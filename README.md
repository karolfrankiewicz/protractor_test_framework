# README #

### What is this repository for? ###

This is repository for storing basic instance of protractor + cucumber framework
Page used for that is angularjs.org

### How do I get set up? ###

Assuming you have node already installed:

1. run "npm install" in command line
2. after installation run "webdriver-manager update"
3. then run "webdriver-manager start"
4. finally run "gulp test:e2e" in separate command line

### Architecture description: ###

This automatic solution is based on "Page Object Pattern" - each web application page/type of page has it's elements and methods stored in separate js files.
For elements that can be found in multiple places in web application there is separate catalogue "elements" which should store those elements (e.g main menu of application).

In this solution "astrolabe" is used for easy maintainig paths to specific pages and for easy importing needed pages in step files. File in which pages are maintained, can be found in "config" catalogue.

Protractor configuration can be found in protractor.config.js file in "config" catalogue

File world.js in "config" catalogue is used for storing constants and methods used by steps (like assertions) so it should be imported in each steps file.

Task for running tests is stored in gulpfile.js.

Feature files contain few sample tests presenting usage of tables and variables passed to steps methods.

Steps and Page files present sample usage of some methods, selectors and assertions.

Solution can be used in running tests by automation mechanisms such as Jenkins tasks.